import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { LOCATIONS, RELATIONS } from '../../../constants';
import { Place, PlaceType, User } from '../../../entity/';
import { getLocationField, setUserToPayload } from '../../../utils';
import { s3FileDelete, uploadToS3 } from '../../../utils/s3';
import { ErrorService } from '../../shared/services/error.service';

@Injectable()
export class PlaceService {
  constructor(
    @InjectRepository(Place)
    private readonly placeRepository: Repository<Place>,
    @InjectRepository(PlaceType)
    private readonly placeTypesRepository: Repository<PlaceType>,
    private readonly errorService: ErrorService,
  ) {}

  async fetchPlaces() {
    const { PlaceType, Festival } = RELATIONS;
    const { Location } = LOCATIONS;
    try {
      const places = await this.placeRepository.find({
        where: { is_active: true },
        relations: [Festival, PlaceType],
      });
      return places.map(place => ({
        ...place,
        ...getLocationField(place, Location),
      }));
    } catch (e) {
      throw this.errorService.handleError(e, 'Error occurred in places');
    }
  }

  async fetchPlaceTypes() {
    try {
      const places = await this.placeTypesRepository.find({ is_active: true });
      return places;
    } catch (e) {
      throw this.errorService.handleError(e, 'Error occurred in place types');
    }
  }

  async createNewPlace(data: Place, user: User) {
    const { Location } = LOCATIONS;

    try {
      if (data.picture_url) {
        const picture = await uploadToS3('places', data.picture_url);
        data.picture_url = picture.Location;
      }
      const createdPlace = await this.placeRepository.save({
        is_active: true,
        ...data,
        ...setUserToPayload(data, user, 'created_by'),
      });
      return {
        is_active: true,
        ...createdPlace,
        ...getLocationField(createdPlace, Location),
      };
    } catch (e) {
      throw this.errorService.handleError(e, 'Error occurred in place');
    }
  }

  async updateExistPlaceById(place_id: number, data: any, user: User) {
    const { Location } = LOCATIONS;

    try {
      if (data.picture_url && data.picture_url.includes('s3') && data.picture) {
        await s3FileDelete(data.picture_url);
      }

      if (data.picture) {
        const picture = await uploadToS3('places', data.picture);
        data.picture_url = picture.Location;
      }

      delete data.picture;

      const updatedPlace = await this.placeRepository.update(
        { id: place_id },
        {
          ...data,
          ...setUserToPayload(data, user, 'updated_by'),
        },
      );

      if (updatedPlace.raw.changedRows) {
        return {
          ...data,
          ...getLocationField(data, Location),
        };
      } else {
        throw this.errorService.handleError(
          {},
          'Place with such Id doesn\'t exist',
        );
      }
    } catch (e) {
      throw this.errorService.handleError(e, 'Error occurred in place');
    }
  }

  async removePlaceById(place_id: number, user: User) {
    try {
      const data: any = {
        is_active: false,
      };

      const updatedPlace = await this.placeRepository.update(
        { id: place_id },
        {
          ...data,
          ...setUserToPayload(data, user, 'updated_by'),
        },
      );

      if (updatedPlace.raw.changedRows) {
        return { place_id };
      } else {
        throw this.errorService.handleError(
          {},
          'Place with such Id doesn\'t exist',
        );
      }
    } catch (e) {
      throw this.errorService.handleError(e, 'Error occurred in place');
    }
  }
}
