import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { Attendee, Place, PlaceType, User } from '../../../entity';
import { UserRepository } from '../../../repositorys';
import { AuthGuard } from '../../auth/guards/auth.guard';
import { ErrorService } from '../../shared/services/error.service';

import { PlaceController } from './place.controller';
import { PlaceService } from './place.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Attendee,
      Place,
      PlaceType,
      User,
      UserRepository,
    ]),
  ],
  controllers: [PlaceController],
  providers: [PlaceService, ErrorService, AuthGuard],
})
export class PlaceModule {}
