import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Req,
  UseGuards,
} from '@nestjs/common';
import {
  ApiImplicitHeader,
  ApiImplicitParam,
  ApiOperation,
  ApiResponse,
  ApiUseTags,
} from '@nestjs/swagger';

import { BAD_REQUEST } from '../../../constants';
import { PlaceDto } from '../../../dto';
import { PlaceTypeDto } from '../../../dto/PlaceTypeDto';
import { AuthGuard } from '../../auth/guards/auth.guard';
import { CustomRequest } from '../../shared/interfaces';
import { ErrorService } from '../../shared/services/error.service';

import { PlaceService } from './place.service';

@Controller('cms/place')
@ApiUseTags('cms/place')
export class PlaceController {
  constructor(
    private readonly placeService: PlaceService,
    private readonly errorService: ErrorService,
  ) {}

  @Get()
  @ApiOperation({ title: 'Get all place data' })
  @ApiResponse({
    status: 200,
    type: PlaceDto,
    description: 'Get all place data',
  })
  @ApiImplicitHeader({
    name: 'Authorization',
    description: 'Bearer + token',
    required: true,
  })
  @UseGuards(AuthGuard)
  async fetchPlaceData(@Req() req: CustomRequest) {
    return this.placeService.fetchPlaces();
  }

  @Get('/types')
  @ApiOperation({ title: 'Get all place data' })
  @ApiResponse({
    status: 200,
    type: PlaceTypeDto,
    description: 'Get all place data',
  })
  @ApiImplicitHeader({
    name: 'Authorization',
    description: 'Bearer + token',
    required: true,
  })
  @UseGuards(AuthGuard)
  async fetchPlaceTypesData(@Req() req: CustomRequest) {
    return this.placeService.fetchPlaceTypes();
  }

  @Post()
  @ApiOperation({ title: 'Create new place' })
  @ApiResponse({
    status: 201,
    type: PlaceDto,
    description: 'Returns created place',
  })
  @ApiImplicitHeader({
    name: 'Authorization',
    description: 'Bearer + token',
    required: true,
  })
  @UseGuards(AuthGuard)
  async createNewPplace(@Req() req: CustomRequest) {
    const { body, user } = req;
    return this.placeService.createNewPlace(body, user);
  }

  @Put('/:place_id')
  @ApiOperation({ title: 'Update exist place by Id' })
  @ApiResponse({
    status: 201,
    type: PlaceDto,
    description: 'Returns updated place',
  })
  @ApiImplicitHeader({
    name: 'Authorization',
    description: 'Bearer + token',
    required: true,
  })
  @ApiImplicitParam({
    name: 'place_id',
    description: 'Place Id',
    required: true,
    type: Number,
  })
  @UseGuards(AuthGuard)
  async updateExistPlaceById(
    @Param('place_id') place_id,
    @Req() req: CustomRequest,
  ) {
    if (!place_id) {
      throw this.errorService.generateError(
        BAD_REQUEST,
        'place_id params is necessary',
      );
    }
    const { body, user } = req;
    return this.placeService.updateExistPlaceById(place_id, body, user);
  }

  @Delete('/:place_id')
  @ApiOperation({ title: 'Remove place by Id' })
  @ApiResponse({
    status: 201,
    type: PlaceDto,
    description: 'Returns status',
  })
  @ApiImplicitHeader({
    name: 'Authorization',
    description: 'Bearer + token',
    required: true,
  })
  @ApiImplicitParam({
    name: 'place_id',
    description: 'Place Id',
    required: true,
    type: Number,
  })
  @UseGuards(AuthGuard)
  async removePlaceById(
    @Param('place_id') place_id,
    @Req() req: CustomRequest,
  ) {
    if (!place_id) {
      throw this.errorService.generateError(
        BAD_REQUEST,
        'place_id params is necessary',
      );
    }
    return this.placeService.removePlaceById(place_id, req.user);
  }
}
